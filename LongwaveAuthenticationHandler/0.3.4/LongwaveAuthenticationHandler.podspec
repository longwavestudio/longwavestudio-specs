#
# Be sure to run `pod lib lint LongwaveAuthenticationHandler.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LongwaveAuthenticationHandler'
  s.version          = '0.3.4'
  s.summary          = 'foshf;dkjf wefjlkfjlkn sadliflwejfnlkef /.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
fkjehfq awefhkwfe ewhfouwehr wehryowe wefyoqweuihr ewkryoiuwqher
                       DESC
  s.swift_versions = '5.0'
  s.homepage         = 'https://bitbucket.org/leppio/longwaveauthenticationhandler/src/master/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'alessio.bonu@gmail.com' => 'alessio.bonu@gmail.com' }
  s.source           = { :git => 'https://leppio@bitbucket.org/leppio/longwaveauthenticationhandler.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.4'

  s.source_files = 'LongwaveAuthenticationHandler/Classes/**/*'
  
  # s.resource_bundles = {
  #   'LongwaveAuthenticationHandler' => ['LongwaveAuthenticationHandler/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'LongwaveUtils', '~> 0.1.0'
  s.dependency 'LongwaveNetworkUtils', '~> 0.2.0'

end
